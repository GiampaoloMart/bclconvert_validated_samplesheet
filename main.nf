#!/usr/bin/env nextflow

params.input_samplesheet = ''
params.input_directory = ''
params.output_directory = ''

process samplesheet_validation {
    label 'pbs'
    publishDir "${params.output_directory}", mode: 'copy'
    
    input:
    
    output:
    path '*.txt'
    path '*.csv', optional: true

    script:
    """
    python_script.py ${params.input_samplesheet} ${params.input_directory} ./
    
    """
}

workflow {
    // Execute the process
    samplesheet_validation()
}

