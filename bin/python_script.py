#!/usr/bin/env python3

import csv
import os
import shutil
import subprocess
import re
import sys

def modify_sample_sheet(input_file, output_file):
    # Check if the input file exists
    if not os.path.isfile(input_file):
        print("The input file does not exist.")
        return
    
    # Copy the input file to create the output file
    shutil.copyfile(input_file, output_file)

def run_bcl_convert(input_directory, output_directory, sample_sheet):   
    # Execute the bcl-convert command as a separate process and capture the output
    command = [
        'bcl-convert',
        '--bcl-input-directory',
        input_directory,
        '--output-directory',
        output_directory,
        '--sample-sheet',
        sample_sheet,
        '--sample-name-column-enabled',
        'false',
        '--bcl-sampleproject-subdirectories',
        'false',
        '--bcl-validate-sample-sheet-only',
        'true',
        '--force',
        'true'
    ]
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return stdout.decode('utf-8'), stderr.decode('utf-8')

def extract_indices_and_lanes_from_error_log(error_log_path):
    indices_and_lanes = []

    # Check if the error log file exists
    if not os.path.isfile(error_log_path):
        print("The error log file does not exist.")
        return

    with open(error_log_path, 'r') as f:
        print("Contents of the error log file:")
        print(f.read())

def print_warning_and_errors_logs(log_file_path):
    if not os.path.isfile(log_file_path):
        print("The log file does not exist.")
        return
    
    with open(log_file_path, 'r') as f:
        print("Contents of the log file:")
        print(f.read())

def write_output_to_txt(output_text, output_file_path, additional_messages=None):
    with open(output_file_path, 'w') as f:
        f.write(output_text)
        if additional_messages:
            f.write("\n\nAdditional messages:\n")
            f.write("\n".join(additional_messages))

def write_final_csv(output_directory, input_samplesheet, input_directory):
    run_id = os.path.basename(input_directory)
    csv_file_path = os.path.join(output_directory, 'final_output.csv')
    with open(csv_file_path, 'w', newline='') as csvfile:
        fieldnames = ['id', 'samplesheet', 'lane', 'flowcell']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        writer.writeheader()
        writer.writerow({'id': run_id, 'samplesheet': input_samplesheet, 'lane': '', 'flowcell': input_directory})

def main():
    if len(sys.argv) != 4:
        print("Usage: python3 main.py input_samplesheet.csv input_directory output_folder")
        return

    input_samplesheet = sys.argv[1]
    input_directory = sys.argv[2]
    output_folder = sys.argv[3]
    output_directory = output_folder
    # List to store all messages
    additional_messages = []

    # Generate the output file as a copy of the input file
    output_samplesheet = input_samplesheet.replace('.csv', '_modified.csv')
    modify_sample_sheet(input_samplesheet, output_samplesheet)

    # Execute the bcl-convert command and capture the output
    bcl_convert_output, bcl_convert_error = run_bcl_convert(input_directory, output_directory, input_samplesheet)

    # Add messages to the list of additional messages
    additional_messages.append(bcl_convert_output)
    additional_messages.append(bcl_convert_error)

    if 'Conversion Complete.' in bcl_convert_output:
        print("Conversion completed successfully.")
        print("Samplesheet OK")

        # Write the output to a .txt file
        write_output_to_txt(bcl_convert_output, os.path.join(output_directory, 'output.txt'), additional_messages)

        # Write the final CSV file
        write_final_csv(output_directory, input_samplesheet, input_directory)

    else:
        print("Conversion failed. Finding indices in the error message.")
        indices_to_modify = re.findall(r'\b[A-Z]{8}\b', bcl_convert_error)
        if indices_to_modify:
            print("Indices found in the error message:", indices_to_modify)
            indices_and_lanes = extract_indices_and_lanes_from_error_log(os.path.join(output_directory, 'Logs', 'Errors.log'))
            if indices_and_lanes:
                print("Indices and lanes found in the error log file:", indices_and_lanes)
                
                # Delete the output directory before making the second call to bcl_convert
                if os.path.exists(output_directory):
                    shutil.rmtree(output_directory)

                # Run bcl-convert again using the modified output file as input
                bcl_convert_output, _ = run_bcl_convert(input_directory, output_directory, output_samplesheet)
                if 'Conversion Complete.' in bcl_convert_output:
                    print("Conversion completed successfully.")
                    print("Samplesheet OK")

                    # Write the final CSV file
                    write_final_csv(output_directory, input_samplesheet, input_directory)
                else:
                    print("Invalid modifications.")
            else:
                print("Indices were not found in the error log file.")
                # Print the content of the error log file
                print_warning_and_errors_logs(os.path.join(output_directory, 'Logs', 'Errors.log'))
        else:
            print("Indices not found in the error message.")
            # Print the content of the error log file
            print_warning_and_errors_logs(os.path.join(output_directory, 'Logs', 'Errors.log'))
            if not os.path.getsize(os.path.join(output_directory, 'Logs', 'Errors.log')):
                print("Error log file empty, printing the output of bcl-convert:")
                print(bcl_convert_error)
                print("MODIFY SAMPLESHEET")
        
        # Write the output to a .txt file even in case of error
        write_output_to_txt(bcl_convert_output, os.path.join(output_directory, 'output.txt'), additional_messages)

if __name__ == "__main__":
    main()